# Signal TLS proxy setup with Ansible

Sets up the [Signal TLS Proxy](https://signal.org/blog/help-iran-reconnect/) service with Ansible.

Only supports CentOS 7 but this will obviously change before its EOL in 2024. Until then, better use something stable and proven.

## How to setup Signal TLS Proxy using Linode and Route53

### Ansible dependencies

#### Python dependencies installed locally

    pip install --user linode_api4 boto

#### Linode inventory plugin from Ansible community collections

    ansible-galaxy collection install community.general

#### Ansible Route53 plugin for DNS

Not covered by this guide is having your ``~/.aws`` credentials setup to work with Route53.

    ansible-galaxy collection install community.aws

#### Roles from [my own Gitlab group](https://gitlab.com/stemid-ansible/roles)

    ansible-galaxy install -r requirements.yml

### Copy the default environment

Name the environment whatever you want, I'm naming it after the VPS provider in this case.

    cp -r inventory/default inventory/linode

### Edit the hosts inventory

Edit the dynamic host inventory config file to determine which VPS types and regions you want to use.

* ``inventory/linode/linode.yml``
* ``groups`` matches the tags on your linodes.
* ``signalproxy`` is a hard coded group name in all playbooks.

```
plugin: linode
regions:
  - ap-northeast
  - ap-west
  - ap-south
  - ap-southeast
  - eu-central
groups:
  - signalproxy
types:
  - g6-nanode-1
  - g6-standard-1
```

### Edit the group vars

* ``inventory/linode/group_vars/all.yml``
* ``linode_group`` is the tag that will be used for all newly created linode instances.
* As of writing (2021-02-08) I am unable to use pre-existing linodes with a matching tag, they must be created by the linode.yml Ansible playbook first.
* ``linode_instances`` defines all your linode instances, see the example in the default environment.
* ``linode_access_token`` must be set.
* [Linode.com guide for creating your personal access token](https://www.linode.com/docs/products/tools/linode-api/guides/get-access-token/).

### Create VPS instances

    ansible-playbook -i inventory/linode/linode.yml linode.yml

At the end you will see the label name and IP of each instance, use them in the next step.

### Create DNS records for instances

    ansible-playbook -i inventory/linode/linode.yml route53.yml

### Add VPS instances to your ssh config

This is a manual step where you have to take the newly created VPS instances and put them into your ``~/.ssh/config`` somehow.

Here is an example;

```
Host lixxx-xx.members.linode.com
  HostName lixxx-xx.members.linode.com
  User root
```

If you set ``LINODE_ACCESS_TOKEN`` in your shell you can use ansible-inventory to see all the variables of your instances. And to verify that the inventory works.

    ansible-inventory -i inventory/linode/linode.yml --graph

### Bootstrap system with bootstrap.yml

Run this when adding new hosts, bootstraps them with necessary tools and setup.

    ansible-playbook -i inventory/linode/linode.yml bootstrap.yml

### Run site.yml

This installs and sets up the proxy, TLS cert and also updates it if there are updates from the upstream repo.

    ansible-playbook -i inventory/linode/linode.yml site.yml

### Reset install

    ansible-playbook -i inventory/linode/linode.yml reset.yml

### Delete instances

Set ``state: absent`` in your group vars ``linode_instances`` list.
